//
//  IOSPlatform.h
//  TestStrip
//
//  Created by Chad Godsey.
//  Copyright 2016 Systematic Consulting Group. All rights reserved.
//
//  This software is provided 'as-is', without any express or implied
//  warranty. In no event will the authors be held liable for any damages
//  arising from the use of this software. Permission is granted to anyone to
//  use this software for any purpose, including commercial applications, and to
//  alter it and redistribute it freely, subject to the following restrictions:
//
//  1. The origin of this software must not be misrepresented; you must not
//     claim that you wrote the original software. If you use this software
//     in a product, an acknowledgment in the product documentation would be
//     appreciated but is not required.
//  2. Altered source versions must be plainly marked as such, and must not be
//     misrepresented as being the original software.
//  3. This notice may not be removed or altered from any source
//     distribution.
//
/**
  * @mainpage TestStrip Mobile API
  * This is the documentation for the mobile teststrip library.  The overview of both iOS and Android APIs will be detailed here.
  * On both platforms, an instance of TestStripAdapter will be created and a delegate object assigned to capture events from the library.
  *
  * @section iOS
  * @see TestStripAdapter
  * @see TestStripLibDelegate
  * @see TestStripFrameStatus
  * @see TestStripFrameError
  *
  * @section Android
  * @see com.dci.teststrip.TestStripAdapter
  * @see com.dci.teststrip.TestStripAdapter.TestStripListener
  * @see com.dci.teststrip.TestStripAdapter.FrameStatus
  * @see com.dci.teststrip.TestStripAdapter.FrameError
  */
