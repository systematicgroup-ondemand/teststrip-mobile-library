package com.dci.teststrip;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.net.MalformedURLException;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import com.microsoft.windowsazure.mobileservices.*;
import com.microsoft.windowsazure.mobileservices.http.ServiceFilterResponse;
import com.microsoft.windowsazure.mobileservices.table.TableOperationCallback;


public class TestStripAdapter {

    /**
     * Scanning run-time errors.
     * Can be used to relay information to user during scan.
     */
    public enum FrameError
    {
        UnknownError,
        InvalidPictureSize,
        Barcode1NotDetected,
        Barcode2NotDetected,
        BarcodeMismatch,
        BarcodeTooSmall,
        BarcodeTooBig,
        BarcodeBadAngle,
        NonSquareBarcode,
        InvalidPeakCount,
        BadColorDeviation,
        SquareDetectionFailed,
        HueLookupFailed,
        StorageSaveFailed,
        BadLighting
    };


    /**
     * Run-time scan status information.
     * Can be used to relay information to user during scan.
     */
    public enum FrameStatus
    {
        StorageSaveFinished,
        FoundBarCode1,
        FoundBarCode2,
        FindingHues,
        FoundHues
    };

    /**
     * Individual pad result states.
     * All but ResultPass are errors.
     */
    public enum TestStripResult
    {
        ResultPass,
        LowAccuracy,
        Undefined,
        LowAlkalinity,
        HighAlkalinity
    };

    /**
     * TestStripListener Interface
     * Used to issue callbacks back to main application.
     * See TestStripAdapter.BuildMap() below for data map structure.
     */
    public interface TestStripListener {
        void ScanSuccess(Map data);
        void FrameError(FrameError error);
        void FrameStatus(FrameStatus status);
    }

    private static final int MY_PERMISSIONS_REQUEST_CAMERA = 0x4001;
    private static final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 0x4002;
    private static final int MY_PERMISSIONS_REQUEST_INTERNET = 0x4003;
    private static final int MY_PERMISSIONS_REQUEST_READ_PHONE_STATE = 0x4004;

    private static final String LOG_TAG = "TestStripAdapter";
    private static TestStripListener _listener = null;
    private static TestStripAdapter sInstance;
    private static FrameError _lastError;
    private static FrameStatus _lastStatus;
    private static Map _lastData;
    private static TestSetup _setup;
    private static TestResult _calibration;
    private static TestStripDataArgument _data;
    private static byte[] _frame;
    private static int _frameWidth;
    private static int _frameHeight;
    private static CameraPreview _preview;

    protected static Activity _activity;
    private static MobileServiceClient _client = null;
    private static boolean _reporting = false;
    private static boolean _demoMode = false;
    private static boolean _outdoorMode = false;

    public TestStripAdapter(Activity activity)
    {
        sInstance = this;
        _activity = activity;
        final TestStripNative nativeWrapper = new TestStripNative();

        if (CheckPermissions(Manifest.permission.CAMERA, MY_PERMISSIONS_REQUEST_CAMERA)) {
            _preview = new CameraPreview(activity, 0, CameraPreview.LayoutMode.FitToParent);
        }

        _calibration = new TestResult();
        _setup = new TestSetup();
        _lastData = new HashMap();
        _lastData.put("BarcodePos", new int[]{0,0});
        _lastData.put("SquarePos", new Object[]{new int[]{0,0}, new int[]{0,0}, new int[]{0,0}, new int[]{0,0}, new int[]{0,0}});
        HashMap meta = new HashMap();
        meta.put("Version", 0);
        meta.put("Manufacturer", 0);
        meta.put("Expiration", "0101");
        meta.put("StoreID", 0);
        _lastData.put("Metadata", meta);
        _lastData.put("SquareCount", 5);
        _lastData.put("Hues", new int[]{0,0,0,0,0});
        _lastData.put("Labels", new String[]{"","","","",""});
        _lastData.put("Values", new String[]{"","","","",""});

        try{
            _client = new MobileServiceClient(
                    "https://dci-teststrip-logging.azurewebsites.net",
                    activity
            );
        }catch(MalformedURLException ex){
            //do exception handling here
            ex.printStackTrace();
        }
    }

    // Use this interface so we can call static methods from JNI and access our instance
    public static final Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            Log.i(TestStripAdapter.LOG_TAG, "got handleMessage");
            if (_listener == null)
                return;
            if(msg.what==1){
                Log.i(TestStripAdapter.LOG_TAG, "handle success");
                _listener.ScanSuccess(_lastData);
            }
            if(msg.what==2){
                Log.i(TestStripAdapter.LOG_TAG, "handle error");
                _listener.FrameError(_lastError);
            }
            if(msg.what==3){
                Log.i(TestStripAdapter.LOG_TAG, "handle status");
                _listener.FrameStatus(_lastStatus);
            }

            super.handleMessage(msg);
        }
    };

    /**
     * Register TestStripListener Interface
     */
    public static void SetListener(TestStripListener listener) {
        _listener = listener;
    }
    public static TestStripListener GetListener() {
        return _listener;
    }

    /**
     * Get Camera Preview layer to add to UI
     */
    public static CameraPreview GetPreview() { return _preview; }

    /**
     * Enable calibration reporting API.
     * Not for use in commercial apps.
     */
    public static void EnableReporting(boolean enable) { _reporting = enable; }

    /**
     * Enable camera flash for poor lighting conditions.
     */
    public static void EnableDemoMode(boolean enable)
    {
        _demoMode = enable;
        _preview.enableTorch(enable);
    }

    /**
     * Enable outdoor detection bins.
     */
    public static void EnableOutdoorMode(boolean enable) {
        _outdoorMode = enable;
        TestStripNative.SetOutdoor(enable);
    }


    /**
     * Initialize internal C++ library
     */
    public void Initialize()
    {
        TestStripNative.Initialize();
    }

    /**
     * Cleanup internal C++ library
     */
    public void Shutdown()
    {
        TestStripNative.Shutdown();
    }

    /**
     * Begin capturing from camera
     */
    public void StartScan()
    {
        _preview.resume();
        _preview.postDelayed(new Runnable() { public void run() { TestStripNative.StartScan(); } }, 1000);
    }

    /**
     * Stop capturing from camera
     */
    public void StopScan()
    {
        _preview.pause();
        TestStripNative.StopScan();
    }



    /**
     * JNI Calls forwarded to TestStripListener
     */
    public static void ScanSuccessEvent(TestStripDataArgument data)
    {
        _preview.pause();
        _data = data;
        BuildMap();

        Message msg = handler.obtainMessage();
        msg.what = 1;
        handler.sendMessage(msg);
    }
    public static void ErrorEvent(int error)
    {
        _lastError = FrameError.values()[error];
        Message msg = handler.obtainMessage();
        msg.what = 2;
        handler.sendMessage(msg);
    }
    public static void StatusEvent(int status)
    {
        _lastStatus = FrameStatus.values()[status];
        Message msg = handler.obtainMessage();
        msg.what = 3;
        handler.sendMessage(msg);
    }


    private static Point scaledPosition(int x, int y)
    {
        float wScale = _preview.getMeasuredWidth()/480.f;
        float hScale = _preview.getMeasuredHeight()/800.f;
        return new Point(
                (int)(_preview.getMeasuredWidth() - y*wScale) + _preview.getOffsetPosition().x,
                (int)(x*hScale) + _preview.getOffsetPosition().y);
    }

    private static void BuildMap()
    {
        _lastData.put("BarcodePos", new Point[]{
                scaledPosition(_data.origin.x + _data.barcodePos[0].x, _data.origin.y + _data.barcodePos[0].y),
                scaledPosition(_data.origin.x + _data.barcodePos[1].x, _data.origin.y + _data.barcodePos[1].y)
        });
        _lastData.put("BarcodeWidth", _data.qrCodeWidth);

        _lastData.put("SquarePos", new Point[]{
                scaledPosition(_data.origin.x + _data.squarePos[0].x, _data.origin.y + _data.squarePos[0].y),
                scaledPosition(_data.origin.x + _data.squarePos[1].x, _data.origin.y + _data.squarePos[1].y),
                scaledPosition(_data.origin.x + _data.squarePos[2].x, _data.origin.y + _data.squarePos[2].y),
                scaledPosition(_data.origin.x + _data.squarePos[3].x, _data.origin.y + _data.squarePos[3].y),
                scaledPosition(_data.origin.x + _data.squarePos[4].x, _data.origin.y + _data.squarePos[4].y)});

        HashMap meta = new HashMap();
        meta.put("Version", _data.version);
        meta.put("Manufacturer", _data.manufacturer);
        meta.put("Expiration", _data.expiration);
        meta.put("StoreID", _data.storeId);
        _lastData.put("Metadata", meta);

        _lastData.put("Hues", new int[]{
                Color.HSVToColor(new float[]{_data.hues[0]*1.f, 1.f, 1.f}),
                Color.HSVToColor(new float[]{_data.hues[1]*1.f, 1.f, 1.f}),
                Color.HSVToColor(new float[]{_data.hues[2]*1.f, 1.f, 1.f}),
                Color.HSVToColor(new float[]{_data.hues[3]*1.f, 1.f, 1.f}),
                Color.HSVToColor(new float[]{_data.hues[4]*1.f, 1.f, 1.f})});

        _lastData.put("Labels", _data.labels);
        _lastData.put("Values", _data.statuses);
        _lastData.put("Results", _data.results);
    }

    /**
     * Send data to Azure mobile services
     * Calibration reporting, not used in commercial apps.
     */
    public static void SendSetup(String notes, double hardness, double cyanuric, double alkalinity, double ph, double chlorine, int luminance)
    {
        _setup.id = null;
        _setup.notes = notes;
        _setup.totalHardness = hardness;
        _setup.cyanuricAcid = cyanuric;
        _setup.totalAlkalinity = alkalinity;
        _setup.ph = ph;
        _setup.freeChlorine = chlorine;
        _setup.totalChlorine = 0.0;
        _setup.luminance = luminance;
        if (_reporting) {

            try{
                _client.getTable(TestSetup.class).insert(_setup, new TableOperationCallback<TestSetup>() {
                    public void onCompleted(TestSetup entity, Exception exception, ServiceFilterResponse response) {
                        if (exception == null) {
                            // Insert succeeded
                            Log.v(LOG_TAG, "Report submit data success");
                            _setup.id = entity.id;
                            //Log.d(LOG_TAG, " XX = " + response.getStatus());
                            //Log.d(LOG_TAG, " YY = " + response.getContent());
                        } else {
                            // Insert failed
                            Log.v(LOG_TAG, "Report submit data error: ");
//                            exception.printStackTrace();
//                            Log.d(LOG_TAG, " toString = " + exception.toString());
//                            Log.d(LOG_TAG, " getCause = " + exception.getCause());
//                            Log.d(LOG_TAG, " getMessage = " + exception.getCause().getMessage());
//                            Log.d(LOG_TAG, " XX = " + response.getStatus());
//                            Log.d(LOG_TAG, " YY = " + response.getContent());
                        }
                    }
                });
            }catch(Exception ex){
                //do exception handling here
                ex.printStackTrace();
            }
        }
    }
    public static void SendReport(String notes)
    {
        _calibration.notes = notes;
        if (_reporting)
            SendData();
    }
    @SuppressLint("MissingPermission")
    private static void SendData()
    {
        final TelephonyManager tm = (TelephonyManager) _activity.getBaseContext().getSystemService(Context.TELEPHONY_SERVICE);

        final String tmDevice, tmSerial, androidId;

        if (!CheckPermissions(Manifest.permission.READ_PHONE_STATE, MY_PERMISSIONS_REQUEST_READ_PHONE_STATE)) {
            return;
        }

        tmDevice = "" + tm.getDeviceId();
        tmSerial = "" + tm.getSimSerialNumber();
        androidId = "" + android.provider.Settings.Secure.getString(_activity.getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);

        UUID deviceUuid = new UUID(androidId.hashCode(), ((long)tmDevice.hashCode() << 32) | tmSerial.hashCode());
        String deviceId = deviceUuid.toString();

        _frame = _data.image;
        _frameWidth = _data.imageWidth;
        _frameHeight = _data.imageHeight;
        Bitmap bmp  = Bitmap.createBitmap(_frameWidth, _frameHeight, Bitmap.Config.ARGB_8888);
        bmp.copyPixelsFromBuffer(ByteBuffer.wrap(_frame));
        ByteArrayOutputStream blob = new ByteArrayOutputStream(bmp.getWidth() * bmp.getHeight());
        bmp.compress(Bitmap.CompressFormat.PNG, 100, blob);

        _calibration.image = Base64.encodeToString(blob.toByteArray(), Base64.DEFAULT);

        _calibration.id = null;
        _calibration.testSetupID = _setup.id;
        _calibration.notes = _setup.notes;
        _calibration.qrCode = _data.qrCode;
        _calibration.qrCode2 = _data.qrCode2;
        _calibration.qrCodeWidth = _data.qrCodeWidth;
        _calibration.qrCodeX = _data.barcodePos[0].x;
        _calibration.qrCodeY = _data.barcodePos[0].y;
        _calibration.qrCode2X = _data.barcodePos[1].x;
        _calibration.qrCode2Y = _data.barcodePos[1].y;

        _calibration.cyanuricAcidX         = _data.squarePos[0].x;
        _calibration.cyanuricAcidY         = _data.squarePos[0].y;
        _calibration.totalHardnessX        = _data.squarePos[1].x;
        _calibration.totalHardnessY        = _data.squarePos[1].y;
        _calibration.phX                   = _data.squarePos[2].x;
        _calibration.phY                   = _data.squarePos[2].y;
        _calibration.totalAlkalinityX      = _data.squarePos[3].x;
        _calibration.totalAlkalinityY      = _data.squarePos[3].y;
        _calibration.totalChlorineX        = _data.squarePos[4].x;
        _calibration.totalChlorineY        = _data.squarePos[4].y;
        _calibration.freeChlorineX = 0;
        _calibration.freeChlorineY = 0;

        _calibration.cyanuricAcidHSV         = _data.hsvs[0];
        _calibration.totalHardnessHSV        = _data.hsvs[1];
        _calibration.phHSV                   = _data.hsvs[2];
        _calibration.totalAlkalinityHSV      = _data.hsvs[3];
        _calibration.totalChlorineHSV        = _data.hsvs[4];
        _calibration.freeChlorineHSV = 0;

        _calibration.cyanuricAcid         = (float)_data.values[0];
        _calibration.totalHardness        = (float)_data.values[1];
        _calibration.ph                   = (float)_data.values[2];
        _calibration.totalAlkalinity      = (float)_data.values[3];
        _calibration.totalChlorine        = (float)_data.values[4];
        _calibration.freeChlorine = 0;

        Log.v(LOG_TAG, "Native SendData " + _calibration.qrCode);
        _calibration.deviceID = deviceId;
        _calibration.modelNumber = Devices.getDeviceName();


        try{
            _client.getTable(TestResult.class).insert(_calibration, new TableOperationCallback<TestResult>() {
                public void onCompleted(TestResult entity, Exception exception, ServiceFilterResponse response) {
                    if (exception == null) {
                        // Insert succeeded
                        Log.v(LOG_TAG, "Report submit data success");
                        //Log.d(LOG_TAG, " XX = " + response.getStatus());
                        //Log.d(LOG_TAG, " YY = " + response.getContent());
                    } else {
                        // Insert failed
                        Log.v(LOG_TAG, "Report submit data error: ");
                        exception.printStackTrace();
//                        Log.d(LOG_TAG, " toString = " + exception.toString());
//                        Log.d(LOG_TAG, " getCause = " + exception.getCause());
//                        Log.d(LOG_TAG, " getMessage = " + exception.getCause().getMessage());
//                        Log.d(LOG_TAG, " XX = " + response.getStatus());
//                        Log.d(LOG_TAG, " YY = " + response.getContent());
                    }
                }
            });
        }catch(Exception ex){
            //do exception handling here
            ex.printStackTrace();
        }
    }


    /**
     * Check if permission is allowed, request permission in background.
     */
    private static boolean CheckPermissions(String permission, int callbackId) {
        if (ContextCompat.checkSelfPermission(_activity,
                permission)
                != PackageManager.PERMISSION_GRANTED) {

            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(_activity,
                    permission)) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                // No explanation needed; request the permission
                ActivityCompat.requestPermissions(_activity,
                        new String[]{permission},
                        callbackId);
            }
            // We do not have permission, will retry upon callbacks.
            return false;
        }
        return true;
    }
    /**
     * Handle permissions requests.
     * Must be called from Activity onRequestPermissionsResult if available.
     */
    public static void RequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_PHONE_STATE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    if (_reporting)
                        SendData();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }
}
