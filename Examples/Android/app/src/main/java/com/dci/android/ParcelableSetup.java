package com.dci.android;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by chadyg on 5/15/2017.
 */

public class ParcelableSetup implements Parcelable {
    public String mNotes;
    public double mTotalHardness;
    public double mTotalChlorine;
    public double mFreeChlorine;
    public double mPh;
    public double mTotalAlkalinity;
    public double mCyanuricAcid;
    public int mLuminance;

    /* everything below here is for implementing Parcelable */

    // 99.9% of the time you can just ignore this
    @Override
    public int describeContents() {
        return 0;
    }

    // write your object's data to the passed-in Parcel
    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeString(mNotes);
        out.writeDouble(mTotalHardness);
        out.writeDouble(mTotalChlorine);
        out.writeDouble(mFreeChlorine);
        out.writeDouble(mPh);
        out.writeDouble(mTotalAlkalinity);
        out.writeDouble(mCyanuricAcid);
        out.writeInt(mLuminance);
    }

    // this is used to regenerate your object. All Parcelables must have a CREATOR that implements these two methods
    public static final Parcelable.Creator<ParcelableSetup> CREATOR = new Parcelable.Creator<ParcelableSetup>() {
        public ParcelableSetup createFromParcel(Parcel in) {
            return new ParcelableSetup(in);
        }

        public ParcelableSetup[] newArray(int size) {
            return new ParcelableSetup[size];
        }
    };

    // example constructor that takes a Parcel and gives you an object populated with its values
    private ParcelableSetup(Parcel in) {
        mNotes = in.readString();
        mTotalHardness = in.readDouble();
        mTotalChlorine = in.readDouble();
        mFreeChlorine = in.readDouble();
        mPh = in.readDouble();
        mTotalAlkalinity = in.readDouble();
        mCyanuricAcid = in.readDouble();
        mLuminance = in.readInt();
    }
    public ParcelableSetup(String notes, double hardness, double cyanuric, double alkalinity, double ph, double chlorine, int luminance) {
        mNotes = notes;
        mTotalHardness = hardness;
        mCyanuricAcid = cyanuric;
        mTotalAlkalinity = alkalinity;
        mPh = ph;
        mFreeChlorine = chlorine;
        mLuminance = luminance;
    }
}
