package com.dci.android;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;

import com.dci.teststrip.TestStripAdapter;

import java.util.HashMap;
import java.util.Map;


/**
 * Debugging class to draw detected results on top of preview
 */
public class PreviewOverlayView extends View {
    private static final String LOG_TAG = "PreviewOverlayView";
    private Paint mPaint = new Paint();
    private Paint[] mSqPaint;
    private int mWidth, mHeight;
    private Map mData;

    public PreviewOverlayView(Context context) {
        super(context);
        getScreenDimensions(context);
    }
    public PreviewOverlayView(Context context, AttributeSet attrs) {
        super(context, attrs);
        getScreenDimensions(context);
    }

    public void stripDetected(Map data) {
        mData = data;
        invalidate();
    }

    public void clearScreen() {
        mData.clear();
        invalidate();
    }

    private void getScreenDimensions(Context context) {

        mData = new HashMap();

        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        mWidth = size.x;
        mHeight = size.y;


        mPaint.setAntiAlias(true);
        mPaint.setDither(true);
        mPaint.setColor(0xFF000000);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        // mPaint.setStrokeCap(Paint.Cap.ROUND);
        mPaint.setStrokeWidth(3);

        mSqPaint = new Paint[]{new Paint(), new Paint(), new Paint(), new Paint(), new Paint()};
        for (int i=0; i<5; i++) {
            mSqPaint[i].setAntiAlias(true);
            mSqPaint[i].setDither(true);
            mSqPaint[i].setColor(0xFFFF0000);
            mSqPaint[i].setStyle(Paint.Style.STROKE);
            mSqPaint[i].setStrokeJoin(Paint.Join.ROUND);
            // mSqPaint[i].setStrokeCap(Paint.Cap.ROUND);
            mSqPaint[i].setStrokeWidth(3);
            mSqPaint[i].setTextSize(40);
            mSqPaint[i].setTextAlign(Paint.Align.RIGHT);
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Log.i(LOG_TAG, "PreviewOverlayView onDraw");

        // Draw data matrices
        if (mData.containsKey("BarcodePos")) {
            Point[] barcodes = (Point[]) mData.get("BarcodePos");
            canvas.drawCircle(barcodes[0].x, barcodes[0].y, 20, mPaint);
            canvas.drawCircle(barcodes[1].x, barcodes[1].y, 20, mPaint);
        }
        else {
            canvas.drawRect(mWidth / 2.0f - mWidth / 12.0f, mHeight / 18.0f, mWidth / 2.0f + mWidth / 12.0f, mHeight - mHeight / 6.0f, mPaint);
        }

        // Draw detected squares
        if (mData.containsKey("SquarePos")) {
            Point[] squares = (Point[]) mData.get("SquarePos");
            String[] labels = (String[]) mData.get("Labels");
            String[] values = (String[]) mData.get("Values");
            int[] results = (int[]) mData.get("Results");
            int[] hues = (int[]) mData.get("Hues");
            for (int i=0; i<5; i++) {
                mSqPaint[i].setColor(hues[i]);
                canvas.drawRect(squares[i].x - 15, squares[i].y - 15, squares[0].x + 15, squares[i].y + 15, mSqPaint[i]);
                canvas.drawText(labels[i], squares[i].x - 75, squares[i].y-15, mSqPaint[i]);
                canvas.drawText(values[i], squares[i].x - 75, squares[i].y+25, mSqPaint[i]);
                if (TestStripAdapter.TestStripResult.values()[results[i]] == TestStripAdapter.TestStripResult.Undefined)
                {
                    canvas.drawText("?", squares[i].x + 75, squares[i].y+25, mSqPaint[i]);
                }
                if (TestStripAdapter.TestStripResult.values()[results[i]] == TestStripAdapter.TestStripResult.LowAccuracy ||
                        TestStripAdapter.TestStripResult.values()[results[i]] == TestStripAdapter.TestStripResult.LowAlkalinity)
                {
                    canvas.drawText("<", squares[i].x + 75, squares[i].y+25, mSqPaint[i]);
                }
            }
        }
    }
}

