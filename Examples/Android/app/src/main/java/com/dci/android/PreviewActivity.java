package com.dci.android;

/* Test Strip imports */
import com.dci.teststrip.TestSetup;
import com.dci.teststrip.TestStripAdapter;
import com.dci.teststrip.CameraPreview;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Map;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class PreviewActivity extends AppCompatActivity implements TestStripAdapter.TestStripListener {

    private CameraPreview mPreview;
    private FrameLayout mLayout;

    private TestStripAdapter mAdapter;
    private Button mStartBtn;
    private Button mCancelBtn;
    private Button mScanBtn;
    private ImageButton mFlashBtn;
    private EditText mNotes;
    private TextView mText;
    private PreviewOverlayView mOverlay;
    private boolean mFlashOn;

    ParcelableSetup mSetup;

    /**
     * TestStripListener Interface
     */

    /**
     * ScanSuccess
     * @param data HashMap of detection data
     */
    @Override
    public void ScanSuccess(Map data) {
        SetStatus("Detected!");
        LinearLayout layout = (LinearLayout) findViewById(R.id.preview_content_controls);
        layout.setVisibility(View.VISIBLE);

        //Stop scanning to prevent multiple detection
        mAdapter.StopScan();
        //See PreviewOverlayView for reading data
        mOverlay.stripDetected(data);
    }

    /**
     * FrameError
     * @param error enum indicating error state
     */
    @Override
    public void FrameError(TestStripAdapter.FrameError error) {
        switch (error) {
            case Barcode1NotDetected:
                SetStatus("Barcode 1 not found");
                break;
            case Barcode2NotDetected:
                SetStatus("Barcode 2 not found");
                break;
            case BarcodeMismatch:
                SetStatus("Barcodes invalid");
                break;
            case BarcodeTooSmall:
                SetStatus("Strip too far away");
                break;
            case BarcodeTooBig:
                SetStatus("Strip too close");
                break;
            case BarcodeBadAngle:
                SetStatus("Hold Strip straight");
                break;
            case SquareDetectionFailed:
                SetStatus("Pad alignment issue, bad strip?");
                break;
            case HueLookupFailed:
                SetStatus("Undefined hues detected");
            case BadLighting:
                SetStatus("Non-uniform lighting");
                break;
        }
    }

    /**
     * FrameStatus
     * @param status enum indicating status state
     */
    @Override
    public void FrameStatus(TestStripAdapter.FrameStatus status) {
        switch (status) {
            case FoundBarCode1:
                SetStatus("Barcode 1 found");
                break;
            case FoundBarCode2:
                SetStatus("Barcode 2 found");
                break;
            case FoundHues:
                SetStatus("Strip Detected");
                break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_preview);

        // Relative and Linear layouts are tested to work.  Support for others can be added at request.
        // Building the UI programmatically here for simplicity.  This is not required
        mLayout = (FrameLayout) findViewById(R.id.frame_layout);// RelativeLayout(this);

        // Text to display on screen for status
        mText = (TextView) findViewById(R.id.status_text);
        mText.setText("For Demonstration purposes only");


        boolean hasCamera = true;
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            SetStatus("Camera Permissions not granted");
            hasCamera = false;
        } else {

            // Test strip adapter
            mAdapter = new TestStripAdapter(this);
            //Report internal test results to remote database for calibration (internal use only)
            TestStripAdapter.EnableReporting(true);
            TestStripAdapter.EnableOutdoorMode(false);
            //Register ourselves for callbacks from library
            TestStripAdapter.SetListener(this);
            mAdapter.Initialize();
            mAdapter.StartScan();

            // Set up preview
            // Set the second argument by your choice.
            // Usually, 0 for back-facing camera, 1 for front-facing camera.
            // If the OS is pre-gingerbread, this does not have any effect.
            mPreview = TestStripAdapter.GetPreview();
            RelativeLayout.LayoutParams previewLayoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
            previewLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, 0);
            previewLayoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
            // Un-comment below lines to specify the size.
            //previewLayoutParams.height = 500;
            //previewLayoutParams.width = 500;

            // Un-comment below line to specify the position.
            //mPreview.setCenterPosition(270, 130);

            mLayout.addView(mPreview, 0, previewLayoutParams);

            // Debug drawing to validate success data
            mOverlay = new PreviewOverlayView(this);
            mLayout.addView(mOverlay);
            mOverlay.bringToFront();
        }

        //Input at bottom
        mNotes = (EditText) findViewById(R.id.notes_text);
        mNotes.setHint("Notes");

        mCancelBtn = (Button) findViewById(R.id.discard_button);
        mCancelBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Restart scan, do not store result in calibration database.
                LinearLayout layout = (LinearLayout) findViewById(R.id.preview_content_controls);
                layout.setVisibility(View.INVISIBLE);
                mNotes.clearFocus();
                mOverlay.clearScreen();
                mAdapter.StartScan();
            }
        });
        mScanBtn = (Button) findViewById(R.id.scan_button);
        mScanBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Restart scan, store result in calibration database.
                LinearLayout layout = (LinearLayout) findViewById(R.id.preview_content_controls);
                layout.setVisibility(View.INVISIBLE);
                mNotes.clearFocus();
                mOverlay.clearScreen();
                mAdapter.StartScan();
                //Calibration reporting, not used in commercial apps.
                //TestStripAdapter.SendReport(mNotes.getText().toString());
            }
        });
        mFlashOn = false;
        mFlashBtn = (ImageButton) findViewById(R.id.flash_button);
        mFlashBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Toggle flash on or off
                mFlashOn = !mFlashOn;
                if (mFlashOn)
                    mFlashBtn.setImageResource(R.drawable.ic_flash_on);
                else
                    mFlashBtn.setImageResource(R.drawable.ic_flash_off);
                TestStripAdapter.EnableDemoMode(mFlashOn);
            }
        });
        LinearLayout layout = (LinearLayout) findViewById(R.id.preview_content_controls);
        layout.setVisibility(View.INVISIBLE);

        //Calibration reporting, not used in commercial apps.
        if (hasCamera) {
//            Intent intent = getIntent();
//            mSetup = intent.getParcelableExtra(FullscreenActivity.EXTRA_MESSAGE);
//            TestStripAdapter.SendSetup(mSetup.mNotes, mSetup.mTotalHardness, mSetup.mCyanuricAcid, mSetup.mTotalAlkalinity, mSetup.mPh, mSetup.mFreeChlorine, mSetup.mLuminance);
        }
    }

    private void SetStatus(String text) {
        mText.setVisibility(View.VISIBLE);
        mText.setText(text);
        mText.postDelayed(new Runnable() { public void run() { mText.setVisibility(View.INVISIBLE); } }, 3000);
    }
}
