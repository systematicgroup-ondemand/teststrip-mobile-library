package com.dci.android;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dci.teststrip.BuildConfig;

import java.util.Map;

/**
 * An example full-screen activity that shows how to interact with the Test Strip library
 */
public class FullscreenActivity extends AppCompatActivity {

    public static final String EXTRA_MESSAGE = "com.dci.android.TEST_SETUP";
    private Button mStartBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fullscreen);


        TextView vText = (TextView) findViewById(R.id.text_version);
        vText.setText(BuildConfig.VERSION_NAME);

        mStartBtn = (Button) findViewById(R.id.start_button);
        mStartBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Perform action on click
                ParcelableSetup setup = new ParcelableSetup(
                        ((EditText) findViewById(R.id.edit_notes)).getText().toString(),
                        Double.parseDouble(((EditText) findViewById(R.id.edit_hardness)).getText().toString()),
                        Double.parseDouble(((EditText) findViewById(R.id.edit_cyanuric)).getText().toString()),
                        Double.parseDouble(((EditText) findViewById(R.id.edit_alkalinity)).getText().toString()),
                        Double.parseDouble(((EditText) findViewById(R.id.edit_ph)).getText().toString()),
                        Double.parseDouble(((EditText) findViewById(R.id.edit_chlorine)).getText().toString()),
                        Integer.parseInt(((EditText) findViewById(R.id.edit_luminance)).getText().toString())
                );

                Intent myIntent = new Intent(FullscreenActivity.this, PreviewActivity.class);
                myIntent.putExtra(EXTRA_MESSAGE, setup); //Optional parameters
                FullscreenActivity.this.startActivity(myIntent);
            }
        });
    }
}
