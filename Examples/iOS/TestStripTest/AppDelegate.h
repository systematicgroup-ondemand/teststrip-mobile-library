//
//  AppDelegate.h
//  TestStripTest
//
//  Created by Systematic Group on 8/31/16.
//  Copyright © 2016 Systematic Group. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

