//
//  ViewController.m
//  TestStripTest
//
//  Created by Systematic Group on 8/31/16.
//  Copyright © 2016 Systematic Group. All rights reserved.
//

#import "ViewController.h"
#import "TestStripLib/IOSPlatform.h"

@implementation ViewController


- (IBAction)hardnessValueChanged:(UIStepper *)sender {
    _hardnessField.text = [NSString stringWithFormat:@"%.0f", [sender value]];
}
- (IBAction)cyanuricValueChanged:(UIStepper *)sender {
    _cyanuricField.text = [NSString stringWithFormat:@"%.0f", [sender value]];
}
- (IBAction)alkalinityValueChanged:(UIStepper *)sender {
    _alkalinityField.text = [NSString stringWithFormat:@"%.0f", [sender value]];
}
- (IBAction)phValueChanged:(UIStepper *)sender {
    _phField.text = [NSString stringWithFormat:@"%.1f", [sender value]];
}
- (IBAction)chlorineValueChanged:(UIStepper *)sender {
    _chlorineField.text = [NSString stringWithFormat:@"%.0f", [sender value]];
}
- (IBAction)luxValueChanged:(UIStepper *)sender {
    _luxField.text = [NSString stringWithFormat:@"%.0f", [sender value]];
}
- (IBAction)sendButtonTouch:(id)sender {
    [_previewControl setMeasurements:[_notesField text]
                         withHardess:[_hardnessField.text doubleValue]
                        withCyanuric:[_cyanuricField.text doubleValue]
                      withAlkalinity:[_alkalinityField.text doubleValue]
                              withPh:[_phField.text doubleValue]
                        withChlorine:[_chlorineField.text doubleValue]
                             withLux:[_luxField.text doubleValue]
     ];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    /* UI Reference */
    _previewControl = [self.storyboard instantiateViewControllerWithIdentifier:@"PreviewControl"];
    
    [_hardnessField setDelegate:self];
    [_cyanuricField setDelegate:self];
    [_alkalinityField setDelegate:self];
    [_phField setDelegate:self];
    [_chlorineField setDelegate:self];
    [_luxField setDelegate:self];
    [_notesField setDelegate:self];
    
    NSDictionary *infoDictionary = [[NSBundle bundleForClass:[TestStripWrapper class]] infoDictionary];
    NSString *version = [infoDictionary valueForKey:(__bridge NSString*)kCFBundleVersionKey];
    [_versionField setText:version];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}

// It is important for you to hide the keyboard
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

// Orientation locked in test app
//-(void)viewDidLayoutSubviews {
//    [super viewDidLayoutSubviews];
//    
//    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
//    switch (orientation) {
//        case UIInterfaceOrientationPortrait:
//            [_wrapper.previewLayer.connection setVideoOrientation:AVCaptureVideoOrientationPortrait];
//            break;
//        case UIInterfaceOrientationPortraitUpsideDown:
//            [_wrapper.previewLayer.connection setVideoOrientation:AVCaptureVideoOrientationPortraitUpsideDown];
//            break;
//        case UIInterfaceOrientationLandscapeLeft:
//            [_wrapper.previewLayer.connection setVideoOrientation:AVCaptureVideoOrientationLandscapeLeft];
//            break;
//        case UIInterfaceOrientationLandscapeRight:
//            [_wrapper.previewLayer.connection setVideoOrientation:AVCaptureVideoOrientationLandscapeRight];
//            break;
//    }
//    
//    _wrapper.previewLayer.frame = self.view.bounds;
//}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(IBAction) showPreview {
    NSLog(@"do something!");
    [self.navigationController pushViewController:_previewControl animated:YES];
}

@end
