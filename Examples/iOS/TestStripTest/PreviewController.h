//
//  PreviewController.h
//  TestStripTest
//
//  Created by Systematic Group on 2/17/17.
//  Copyright © 2017 Systematic Group. All rights reserved.
//

#ifndef PreviewController_h
#define PreviewController_h

#import <UIKit/UIKit.h>
#import "TestStripLib/IOSPlatform.h"

@interface PreviewController<TestStripLibDelegate, UITextFieldDelegate> : UIViewController

@property TestStripWrapper *wrapper;
@property UILabel *statusLabel;
@property UIScrollView *detectView;
@property UIButton *scanButton;
@property UIButton *cancelButton;
@property UISwitch *chck1;
@property UISwitch *chck2;
@property UISwitch *chck3;
@property UISwitch *chck4;
@property UISwitch *chck5;
@property UITextField *notes;
@property UIImageView *flashIcon;
@property bool demoOn;

@property NSString *notesValue;
@property double hardnessValue;
@property double cyanuricValue;
@property double alkalinityValue;
@property double phValue;
@property double chlorineValue;
@property double luxValue;

@property IBOutlet UITextField *notesField;
@property IBOutlet UITextField *hardnessField;
@property IBOutlet UITextField *cyanuricField;
@property IBOutlet UITextField *alkalinityField;
@property IBOutlet UITextField *phField;
@property IBOutlet UITextField *chlorineField;
@property IBOutlet UITextField *luxField;
-(IBAction) showSetup;

-(void)setMeasurements:(NSString*)notes withHardess:(double)hardness withCyanuric:(double)cyanuric withAlkalinity:(double)alkalinity withPh:(double)ph withChlorine:(double)chlorine withLux:(double)lux;

@end


#endif /* PreviewController_h */
