//
//  ViewController.h
//  TestStripTest
//
//  Created by Systematic Group on 8/31/16.
//  Copyright © 2016 Systematic Group. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PreviewController.h"

@interface ViewController<UITextFieldDelegate> : UITableViewController

@property UITextField *notes;
-(IBAction) showPreview;
@property PreviewController *previewControl;

@property IBOutlet UITextField *versionField;
@property IBOutlet UITextField *notesField;
@property IBOutlet UITextField *hardnessField;
@property IBOutlet UITextField *cyanuricField;
@property IBOutlet UITextField *alkalinityField;
@property IBOutlet UITextField *phField;
@property IBOutlet UITextField *chlorineField;
@property IBOutlet UITextField *luxField;
@property IBOutlet UIButton *startButton;

@end

