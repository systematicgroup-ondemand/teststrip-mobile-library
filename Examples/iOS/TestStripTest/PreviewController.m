//
//  PreviewController.m
//  TestStripTest
//
//  Created by Systematic Group on 2/17/17.
//  Copyright © 2017 Systematic Group. All rights reserved.
//

#import "PreviewController.h"

@implementation PreviewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view setNeedsLayout];
    [self.view layoutIfNeeded];
    
    /* Library Initialization */
    
    // Initialize the library wrapper, set ourselves as the delegate, and add the preview layer to
    // our view
    _wrapper = [[TestStripWrapper alloc] init];
    [_wrapper initialize];
    [_wrapper setDelegate:self];
    //Calibration reporting, not used in commercial apps.
    [_wrapper enableReporting:false];
    [_wrapper enablePrintable:true];
    [_wrapper enableDemoMode:false];
    [_wrapper enableOutdoorMode:true];
    
    _wrapper.previewLayer.frame = self.view.frame;
    [self.view.layer addSublayer:_wrapper.previewLayer];
    //TODO: Determine aspect ratio and alter gravity when screen is taller than photo is wide
    _wrapper.previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    
    // Start scanning
    [_wrapper startScan];
    
    
    /* Build demo UI */
    _detectView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    _detectView.pagingEnabled = YES;
    _detectView.showsHorizontalScrollIndicator = YES;
    _detectView.showsVerticalScrollIndicator = YES;
    _detectView.scrollsToTop = NO;
    _detectView.delegate = self;
    _detectView.contentSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height);
    
    CGFloat width = CGRectGetWidth(self.view.bounds);
    CGFloat height = CGRectGetHeight(self.view.bounds);
    
    // Display status/error on screen
    CGFloat widthlabel = width - (width / 4);
    CGRect frame = CGRectMake(width / 2 - widthlabel / 2, height - 150, widthlabel, 40);
    _statusLabel = [[UILabel alloc] initWithFrame:frame];
    _statusLabel.text = @"For Demonstration purposes only.";
    _statusLabel.numberOfLines = 1;
    _statusLabel.backgroundColor = [UIColor whiteColor];
    _statusLabel.textColor = [UIColor blackColor];
    _statusLabel.textAlignment = NSTextAlignmentCenter;
    _statusLabel.baselineAdjustment = UIBaselineAdjustmentAlignBaselines;
    _statusLabel.alpha = 0;
    [[_statusLabel layer] setCornerRadius:10.f];
    [_statusLabel setAdjustsFontSizeToFitWidth:YES];
    //[_statusLabel setFont:[UIFont systemFontOfSize:20]];
    [self.view addSubview:_statusLabel];
    
    // Button to start scanning
    _scanButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [_scanButton addTarget:self
                    action:@selector(scanClicked:)
          forControlEvents:UIControlEventTouchUpInside];
    [_scanButton setTitle:@"Scan" forState:UIControlStateNormal];
    [[_scanButton layer] setCornerRadius:4.f];
    [_scanButton setBackgroundColor:self.view.tintColor];
    _scanButton.frame = CGRectMake(220.0, height - 150, 120.0, 40.0);
    [_detectView addSubview:_scanButton];
    
    // Button to cancel scanning
    _cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [_cancelButton addTarget:self
                    action:@selector(cancelClicked:)
          forControlEvents:UIControlEventTouchUpInside];
    [_cancelButton setTitle:@"Discard" forState:UIControlStateNormal];
    [[_cancelButton layer] setCornerRadius:4.f];
    [_cancelButton setTitleColor:self.view.tintColor forState:UIControlStateNormal];
    [_cancelButton setBackgroundColor:[UIColor whiteColor]];
    _cancelButton.frame = CGRectMake(40.0, height - 150, 120.0, 40.0);
    [_detectView addSubview:_cancelButton];
    
//    _chck1 = [[UISwitch alloc] initWithFrame:CGRectMake(250.0, 100.0, 50.0, 40.0)];
//    [_detectView addSubview:_chck1];
//    _chck2 = [[UISwitch alloc] initWithFrame:CGRectMake(250.0, 200.0, 50.0, 40.0)];
//    [_detectView addSubview:_chck2];
//    _chck3 = [[UISwitch alloc] initWithFrame:CGRectMake(250.0, 300.0, 50.0, 40.0)];
//    [_detectView addSubview:_chck3];
//    _chck4 = [[UISwitch alloc] initWithFrame:CGRectMake(250.0, 400.0, 50.0, 40.0)];
//    [_detectView addSubview:_chck4];
//    _chck5 = [[UISwitch alloc] initWithFrame:CGRectMake(250.0, 500.0, 50.0, 40.0)];
//    [_detectView addSubview:_chck5];
//    
//    _notes = [[UITextField alloc] initWithFrame:CGRectMake(width/8, height - 100, width*3/4, 40)];
//    _notes.backgroundColor = [UIColor whiteColor];
//    _notes.textColor = [UIColor blackColor];
//    _notes.placeholder = @"Enter notes";
//    [_notes setReturnKeyType:UIReturnKeyDone];
//    [_notes setDelegate:self];
//    [_detectView addSubview:_notes];
    
    //Flash toggle
    _flashIcon = [[UIImageView alloc] initWithImage: [UIImage imageNamed:@"flash_off"]];
    [_flashIcon setUserInteractionEnabled:true];
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                 action:@selector(flashTapHandler:)];
    [_flashIcon addGestureRecognizer:tapGesture];
    _flashIcon.frame = CGRectMake(width - 48.0, 16.0, 32.0, 32.0);
    [self.view addSubview:_flashIcon];
    _demoOn = false;
    
    
    [_detectView setHidden:TRUE];
    [self.view addSubview:_detectView];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if ((self.navigationController) && ([self.navigationController.viewControllers containsObject:self])) {
        NSLog(@"I've been pushed!");
        [_wrapper startScan];
        //Calibration reporting, not used in commercial apps.
        //[_wrapper sendSetup:_notesValue hardnessValue:_hardnessValue cyanuricValue:_cyanuricValue alkalinityValue:_alkalinityValue phValue:_phValue chlorineValue:_chlorineValue luminanceValue:(int)_luxValue];
        [_wrapper enableOutdoorMode:(bool)(_luxValue >= 57000)];
        [_wrapper enableIndoorMode:(bool)(_luxValue < 57000)];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    if ((self.navigationController) && (![self.navigationController.viewControllers containsObject:self])) {
        NSLog(@"I've been popped!");
        [_wrapper stopScan];
    }
}


-(void)setMeasurements:(NSString*)notes withHardess:(double)hardness withCyanuric:(double)cyanuric withAlkalinity:(double)alkalinity withPh:(double)ph withChlorine:(double)chlorine withLux:(double)lux {
    NSLog(@"Got Measurements");
    self.notesValue = notes;
    self.hardnessValue = hardness;
    self.cyanuricValue = cyanuric;
    self.alkalinityValue = alkalinity;
    self.phValue = ph;
    self.chlorineValue = chlorine;
    self.luxValue = lux;
}

// Orientation locked in test app
//-(void)viewDidLayoutSubviews {
//    [super viewDidLayoutSubviews];

//
//    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
//    switch (orientation) {
//        case UIInterfaceOrientationPortrait:
//            [_wrapper.previewLayer.connection setVideoOrientation:AVCaptureVideoOrientationPortrait];
//            break;
//        case UIInterfaceOrientationPortraitUpsideDown:
//            [_wrapper.previewLayer.connection setVideoOrientation:AVCaptureVideoOrientationPortraitUpsideDown];
//            break;
//        case UIInterfaceOrientationLandscapeLeft:
//            [_wrapper.previewLayer.connection setVideoOrientation:AVCaptureVideoOrientationLandscapeLeft];
//            break;
//        case UIInterfaceOrientationLandscapeRight:
//            [_wrapper.previewLayer.connection setVideoOrientation:AVCaptureVideoOrientationLandscapeRight];
//            break;
//    }
//
//    _wrapper.previewLayer.frame = self.view.bounds;
//}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    NSLog(@"I've been freed!");
    
    [_wrapper shutdown];
}

-(void) scanClicked:(UIButton*)sender
{
    // Reset scanning UI and start scanning
    [_wrapper startScan];
    [_notes resignFirstResponder];
    [_detectView setHidden:TRUE];
    [_statusLabel setHidden:FALSE];
    [self clearShapes];
    _statusLabel.text = @"";
    
    //Calibration reporting, not used in commercial apps.
    //[_wrapper sendReport:_chck1.isOn square2Valid:_chck2.isOn square3Valid:_chck3.isOn square4Valid:_chck4.isOn square5Valid:_chck5.isOn notes:_notesValue];
}

-(void) cancelClicked:(UIButton*)sender
{
    // Reset scanning UI and start scanning
    [_wrapper startScan];
    [_notes resignFirstResponder];
    [_detectView setHidden:TRUE];
    [_statusLabel setHidden:FALSE];
    [self clearShapes];
    _statusLabel.text = @"";
}

/* UITextViewDelegate Methods */

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    
    // Reset scanning UI and start scanning
    [_wrapper startScan];
    [_notes resignFirstResponder];
    [_detectView setHidden:TRUE];
    [_statusLabel setHidden:FALSE];
    [self clearShapes];
    _statusLabel.text = @"";
    
    //Calibration reporting, not used in commercial apps.
    //[_wrapper sendReport:!_chck1.isOn square2Valid:!_chck2.isOn square3Valid:!_chck3.isOn square4Valid:!_chck4.isOn square5Valid:!_chck5.isOn notes:_notes.text];
    
    return NO;
}

/* TestStripLibDelegate Methods */

- (void)scanSuccess:(NSDictionary *)data {
    // The library has detected a strip, from here we can do last minute sanity checking on the data
    // to determine if we should continue scanning
    // For this demo, we will stop scanning and draw the detection data on the last rendered preview
    // frame.  (The frame may not be the one scanned, so some drift may occur if the strip is moving)
    _statusLabel.text = @"Scan Success";
    [_wrapper stopScan];
    [_detectView setHidden:FALSE];
    [_statusLabel setHidden:TRUE];
    NSLog(@"detection: %@", data);
    
    [self clearShapes];
    
    // Draw the barcode positions
    [self drawCircle:[data[@"BarcodePos"][0][@"X"] integerValue] y:[data[@"BarcodePos"][0][@"Y"] integerValue] color:[UIColor redColor]];
    [self drawCircle:[data[@"BarcodePos"][1][@"X"] integerValue] y:[data[@"BarcodePos"][1][@"Y"] integerValue] color:[UIColor blueColor]];
    
    
    // Debugging output, this data is not normally exported
    //    for (int i=0; i<=1; i++) {
    //        [self drawRect:[data[@"PeakPos"][i][@"X"] integerValue] y:[data[@"PeakPos"][i][@"Y"] integerValue] color:[UIColor greenColor]];
    //    }
    
    // In the Calibration UI, we want our keys in a specific order, so we specify them manually.
    NSArray* keys = [data[@"Detection"][@"Results"] allKeys];
    NSNumber* manufn = data[@"Metadata"][@"Manufacturer"];
    TestStripManufacturer manuf = (TestStripManufacturer)(manufn);
    if ([manufn isEqualToNumber:@(TestStripManufacturerLamotte)])
    {
        //This manufacturer is currently not used, but determining manufacturer to specify key order and count is left in for completeness.
        keys = [NSArray arrayWithObjects:
                @"CyanuricAcid",
                @"TotalHardness",
                @"pH",
                @"TotalAlkalinity",
                @"FreeChlorine",
                nil];
    }
    else
    {
        keys = [NSArray arrayWithObjects:
                @"TotalHardness",
                @"CyanuricAcid",
                @"TotalAlkalinity",
                @"pH",
                @"FreeChlorine",
                nil];
    }
    
    // Draw the detected square positions, along with labels colored to detected hue
    for (int i=0; i< [data[@"SquarePos"] count]; i++) {
        //Red square around pad positions.
        [self drawRect:[data[@"SquarePos"][i][@"X"] integerValue] y:[data[@"SquarePos"][i][@"Y"] integerValue] color:[UIColor redColor]];
        
        //Use detected hue for text label color alongside each pad.
        //Print pad name (key) and status string, which is either double value of ppm result or an error string.
        //Status is a convenience printable string.  The same information is contained in the Results enumeration and values array used below.
        CGFloat grad = [data[@"Detection"][@"Hues"][keys[i]] integerValue]/360.f;
        UIColor *color = [UIColor colorWithHue:grad saturation:1.f brightness:1.f alpha: 1.f];
        [self drawText:[data[@"SquarePos"][i][@"X"] integerValue] y:[data[@"SquarePos"][i][@"Y"] integerValue] color:color text:keys[i]];
        [self drawText:[data[@"SquarePos"][i][@"X"] integerValue] y:[data[@"SquarePos"][i][@"Y"] integerValue] + 20 color:color text:data[@"Detection"][@"Statuses"][keys[i]]];
        
        //Use Detection Results to check individual test result errors.
        //Undefined is a non-capture.
        if ([data[@"Detection"][@"Results"][keys[i]] integerValue] == TestStripResultUndefined)
            [self drawText:[data[@"SquarePos"][i][@"X"] integerValue] + 10 y:[data[@"SquarePos"][i][@"Y"] integerValue] color:[UIColor blackColor] text:@"?"];
        
        //Low Accuracy is a result with low statistical relevance.
        if ([data[@"Detection"][@"Results"][keys[i]] integerValue] == TestStripResultLowAccuracy)
            [self drawText:[data[@"SquarePos"][i][@"X"] integerValue] + 10 y:[data[@"SquarePos"][i][@"Y"] integerValue] color:[UIColor blackColor] text:@"<"];
        
        //Low Alkalinity is a pH specific error, pH results are erratic at low Alkalinity and are not provided.
        if ([data[@"Detection"][@"Results"][keys[i]] integerValue] == TestStripResultLowAlkalinity)
            [self drawText:[data[@"SquarePos"][i][@"X"] integerValue] + 10 y:[data[@"SquarePos"][i][@"Y"] integerValue] color:[UIColor blackColor] text:@"<"];
        
        //Debugging output
        NSLog(@"detection key: %@", keys[i]);
        NSLog(@"detection id: %@", data[@"Detection"][@"Values"][keys[i]]);
        NSLog(@"detection doublevalue: %f", [data[@"Detection"][@"Values"][keys[i]] doubleValue]);
        NSLog(@"detection class: %@", [data[@"Detection"][@"Values"][keys[i]] class]);
    }
    
    // Debugging output, this data is not normally exported
    //    int max = -255;
    //    int min = 255;
    //    for (int i=0; i<[data[@"PeakVals"] count]; i++) {
    //        if ([data[@"PeakVals"][i] integerValue] > max)
    //            max = [data[@"PeakVals"][i] integerValue];
    //        if ([data[@"PeakVals"][i] integerValue] < min)
    //            min = [data[@"PeakVals"][i] integerValue];
    //    }
    //
    //    for (int i=0; i<[data[@"PeakVals"] count]; i++) {
    //        CGFloat grad = ([data[@"PeakVals"][i] integerValue] - min) * (1.f / (max - min));
    //        UIColor *color = [UIColor colorWithRed:grad green:grad blue:grad alpha: 1.f];
    //        [self drawBar:[data[@"PeakPoints"][i][@"X"] integerValue] y:[data[@"PeakPoints"][i][@"Y"] integerValue]
    //            color:color];
    //    }
    
    //FIXME: Endurance Testing
    //[self scanClicked:NULL];
    //[self cancelClicked:NULL];
}

- (void)frameError:(TestStripFrameError)error {
    // Display error on our label
    switch (error) {
        case TestStripInvalidPictureSize:
            _statusLabel.text = @"Invalid Picture Size";
            break;
        case TestStripBarcode1NotDetected:
            _statusLabel.text = @"Barcode not found";
            break;
        case TestStripBarcode2NotDetected:
            _statusLabel.text = @"Barcode not found";
            break;
        case TestStripBarcodeMismatch:
            _statusLabel.text = @"Invalid barcodes";
            break;
        case TestStripBarcodeTooSmall:
            _statusLabel.text = @"Too far away";
            break;
        case TestStripBarcodeTooBig:
            _statusLabel.text = @"Too close";
            break;
        case TestStripBarcodeBadAngle:
            _statusLabel.text = @"Hold strip straight";
            break;
        case TestStripSquareDetectionFailed:
            _statusLabel.text = @"Pad alignment issue, bad strip?";
            break;
        case TestStripHueLookupFailed:
            _statusLabel.text = @"Undefined hues detected";
            break;
        case TestStripBadLighting:
            _statusLabel.text = @"Non-uniform lighting";
            break;
            //...
            
    }
    [self fadein];
}

- (void)frameStatus:(TestStripFrameStatus)status {
    // Display status on our label
    switch (status) {
        case TestStripFoundBarCode1:
            _statusLabel.text = @"Found barcode 1";
            break;
        case TestStripFoundBarCode2:
            _statusLabel.text = @"Found barcode 2";
            break;
        case TestStripFoundHues:
            _statusLabel.text = @"Found hues";
            break;
    }
    [self fadein];
}

-(void)reportSuccess {
    _statusLabel.text = @"Report Stored";
    [self fadein];
}

-(void)reportFailed:(NSString*)message {
    NSString *alertMsg = [NSString stringWithFormat:@"%@\n%@", @"Check Network Connection and try again.", message];
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Report Storage Failed"
                                                                   message: alertMsg
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    UIAlertAction* retryAction = [UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * action) { [_wrapper retryReport]; }];
    
    [alert addAction:cancelAction];
    [alert addAction:retryAction];
    [self presentViewController:alert animated:YES completion:nil];
}


/* label fade in, just to be fancy */
-(void) fadein
{
    _statusLabel.alpha = 0;
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
    
    //don't forget to add delegate.....
    [UIView setAnimationDelegate:self];
    
    [UIView setAnimationDuration:0.5];
    _statusLabel.alpha = 1;
    
    //also call this before commit animations......
    [UIView setAnimationDidStopSelector:@selector(animationDidStop:finished:context:)];
    [UIView commitAnimations];
    
    [[self view] setNeedsDisplay];
    [[NSRunLoop currentRunLoop] runUntilDate: [NSDate dateWithTimeIntervalSinceNow: 0.01]];
}

-(void)animationDidStop:(NSString *)animationID finished:(NSNumber *)finished    context:(void *)context
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:5];
    _statusLabel.alpha = 0;
    [UIView commitAnimations];
    
    [[self view] setNeedsDisplay];
    [[NSRunLoop currentRunLoop] runUntilDate: [NSDate dateWithTimeIntervalSinceNow: 0.01]];
}

/* helper draw functions */

- (void)clearShapes {
    for (CALayer *layer in [self.view.layer.sublayers copy]) {
        if ([layer isKindOfClass:[CAShapeLayer class]]||
            [layer isKindOfClass:[CATextLayer class]])
            [layer removeFromSuperlayer];
    }
}

- (void)drawCircle:(double)x y:(double)y color:(UIColor*)color {
    CAShapeLayer *circleLayer = [CAShapeLayer layer];
    [circleLayer setPath:[[UIBezierPath bezierPathWithOvalInRect:CGRectMake(x-5, y-5, 10, 10)] CGPath]];
    [circleLayer setStrokeColor:[color CGColor]];
    [circleLayer setFillColor:[[UIColor clearColor] CGColor]];
    [[self.view layer] addSublayer:circleLayer];
    //[circleLayer release];
}

- (void)drawRect:(double)x y:(double)y color:(UIColor*)color {
    CAShapeLayer *circleLayer = [CAShapeLayer layer];
    [circleLayer setPath:[[UIBezierPath bezierPathWithRect:CGRectMake(x-5, y-5, 10, 10)] CGPath]];
    [circleLayer setStrokeColor:[color CGColor]];
    [circleLayer setFillColor:[[UIColor clearColor] CGColor]];
    [[self.view layer] addSublayer:circleLayer];
    //[circleLayer release];
}

- (void)drawBar:(double)x y:(double)y color:(UIColor*)color {
    CAShapeLayer *circleLayer = [CAShapeLayer layer];
    [circleLayer setPath:[[UIBezierPath bezierPathWithRect:CGRectMake(x-20, y-5, 40, 10)] CGPath]];
    [circleLayer setStrokeColor:[color CGColor]];
    [circleLayer setFillColor:[[UIColor clearColor] CGColor]];
    [[self.view layer] addSublayer:circleLayer];
    //[circleLayer release];
}

- (void)drawText:(double)x y:(double)y color:(UIColor*)color text:(NSString*)text {
    CATextLayer *label = [[CATextLayer alloc] init];
    [label setFont:@"Helvetica-Bold"];
    [label setFontSize:20];
    [label setFrame:CGRectMake(x-220, y-15, 200, 30)];
    [label setString:text];
    [label setAlignmentMode:kCAAlignmentRight];
    [label setForegroundColor:[color CGColor]];
    [[self.view layer] addSublayer:label];
    //[label release];
}

- (void)flashTapHandler: (UITapGestureRecognizer *)recognizer
{
    _demoOn = !_demoOn;
    if (_demoOn)
        [_flashIcon setImage:[UIImage imageNamed:@"flash_on"]];
    else
        [_flashIcon setImage:[UIImage imageNamed:@"flash_off"]];
    [_wrapper enableDemoMode:_demoOn];
}

@end
