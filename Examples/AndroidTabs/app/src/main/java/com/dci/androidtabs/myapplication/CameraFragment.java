package com.dci.androidtabs.myapplication;

/* Test Strip imports */
import com.dci.teststrip.TestStripAdapter;
import com.dci.teststrip.CameraPreview;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link CameraFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link CameraFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CameraFragment extends Fragment
        implements TestStripAdapter.TestStripListener {

    public CameraPreview mPreview;
    private TestStripAdapter mAdapter;
    private RelativeLayout mLayout;
    private Map mScanData;

    private OnFragmentInteractionListener mListener;

    public CameraFragment() {
        // Required empty public constructor
    }

    /**
     * TestStripListener Interface
     */

    /**
     * ScanSuccess
     * @param data HashMap of detection data
     */
    @Override
    public void ScanSuccess(Map data) {
//        Snackbar.make(view, "Detected!", Snackbar.LENGTH_LONG)
//                .setAction("Action", null).show();

        //mText.setText("Detected!");
        //mScanBtn.setVisibility(View.VISIBLE);
        mScanData = data;
        mAdapter.StopScan();
        //mOverlay.stripDetected(data);
    }

    /**
     * FrameError
     * @param error enum indicating error state
     */
    @Override
    public void FrameError(TestStripAdapter.FrameError error) {
        switch (error) {
            case Barcode1NotDetected:
                //mText.setText("Barcode 1 not found");
                break;
            case Barcode2NotDetected:
                //mText.setText("Barcode 2 not found");
                break;
            case BarcodeMismatch:
                //mText.setText("Barcodes invalid");
                break;
            case BarcodeTooSmall:
                //mText.setText("Strip too far away");
                break;
            case BarcodeTooBig:
                //mText.setText("Strip too close");
                break;
            case BarcodeBadAngle:
                //mText.setText("Hold Strip straight");
                break;
        }
    }

    /**
     * FrameStatus
     * @param status enum indicating status state
     */
    @Override
    public void FrameStatus(TestStripAdapter.FrameStatus status) {
        switch (status) {
            case FoundBarCode1:
                //mText.setText("Barcode 1 found");
                break;
            case FoundBarCode2:
                //mText.setText("Barcode 2 found");
                break;
            case FindingHues:
                //mText.setText("Strip Detected");
                break;
        }
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment CameraFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CameraFragment newInstance() {
        CameraFragment fragment = new CameraFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        RelativeLayout relativeLayout = new RelativeLayout(this.getActivity());

        // Test strip adapter
        mAdapter = new TestStripAdapter(getActivity());
        //Report internal test results to remote database for calibration (optional)
        mAdapter.EnableReporting(false);
        //Register ourselves for callbacks from library
        mAdapter.SetListener(this);
        mAdapter.Initialize();
        mAdapter.StartScan();


        // Set up preview
        mPreview = mAdapter.GetPreview();
        RelativeLayout.LayoutParams previewLayoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        // Un-comment below lines to specify the size.
        //previewLayoutParams.height = 500;
        //previewLayoutParams.width = 500;

        // Un-comment below line to specify the position.
        //mPreview.setCenterPosition(270, 130);


        mListener.onSetAdapter(mAdapter);
        mListener.onSetPreview(mPreview);

        relativeLayout.addView(mPreview, 0, previewLayoutParams);

        return relativeLayout;

        //SCG Edit: building above programmatically instead of inflating layout below
        // Inflate the layout for this fragment
        //return inflater.inflate(R.layout.fragment_camera, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {

        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            //throw new RuntimeException(context.toString()
            //        + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onSetAdapter(TestStripAdapter adapter);
        void onSetPreview(CameraPreview preview);
    }
}
