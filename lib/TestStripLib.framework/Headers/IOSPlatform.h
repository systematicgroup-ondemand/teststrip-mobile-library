//
//  IOSPlatform.h
//  TestStrip
//
//  Created by Chad Godsey.
//  Copyright 2016 Systematic Consulting Group. All rights reserved.
//
//  This software is provided 'as-is', without any express or implied
//  warranty. In no event will the authors be held liable for any damages
//  arising from the use of this software. Permission is granted to anyone to
//  use this software for any purpose, including commercial applications, and to
//  alter it and redistribute it freely, subject to the following restrictions:
//
//  1. The origin of this software must not be misrepresented; you must not
//     claim that you wrote the original software. If you use this software
//     in a product, an acknowledgment in the product documentation would be
//     appreciated but is not required.
//  2. Altered source versions must be plainly marked as such, and must not be
//     misrepresented as being the original software.
//  3. This notice may not be removed or altered from any source
//     distribution.
//
#import <AVFoundation/AVFoundation.h>

/**
 * @brief NS_ENUM TestStripManufacturer
 * @description Enumerated values for test strip identification status states
 */
typedef NS_ENUM(NSUInteger, TestStripManufacturer) {
    TestStripManufacturerLamotte,
    TestStripManufacturerHach
};

/**
 * @brief NS_ENUM TestStripResult
 * @description Enumerated values for test strip individual test result
 */
typedef NS_ENUM(NSUInteger, TestStripResult) {
    TestStripResultPass,
    TestStripResultLowAccuracy,
    TestStripResultUndefined,
    TestStripResultLowAlkalinity,
    TestStripResultHighAlkalinity
};

/**
 * @brief NS_ENUM TestStripFrameStatus
 * @description Enumerated values for test strip identification status states
 */
typedef NS_ENUM(NSUInteger, TestStripFrameStatus) {
    TestStripStorageSaveFinished, //Used for calibration projects
    TestStripFoundBarCode1,
    TestStripFoundBarCode2,
    TestStripFindingHues,
    TestStripFoundHues
};

/**
 * @brief NS_ENUM TestStripFrameError
 * @description Enumerated values for test strip identification error states
 */
typedef NS_ENUM(NSUInteger, TestStripFrameError) {
    TestStripUnknownError,
    TestStripInvalidPictureSize,
    TestStripBarcode1NotDetected,
    TestStripBarcode2NotDetected,
    TestStripBarcodeMismatch,
    TestStripBarcodeTooSmall,
    TestStripBarcodeTooBig,
    TestStripBarcodeBadAngle,
    TestStripNonSquareBarcode,
    TestStripInvalidPeakCount,
    TestStripBadColorDeviation,
    TestStripSquareDetectionFailed,
    TestStripHueLookupFailed,
    TestStripStorageSaveFailed,
    TestStripBadLighting
};

/**
 * @brief TestStripLibDelegate
 * @description This protocol defines an interface for delegates of TestStripWrapper object to
 * recieve notifications about status and error state changes.
 */
@protocol TestStripLibDelegate <NSObject>
@optional
/**
 * @brief scanSuccess
 * @description This message is recieved when identification and classification of a strip have completed.
 * @param data A Dictionary containing strip metadata, identified hues, bin lookup data
 * @code
 * {
 *     "Barcode" : [
 *         {
 *             "X": Integer x,
 *             "Y": Integer y
 *         },
 *         ...
 *     ],
 *     "SquarePos": [
 *         {
 *             "X": Integer x,
 *             "Y": Integer y
 *         },
 *         ...
 *     ],
 *     "Metadata": {
 *         "Version": Integer version,
 *         "Manufacturer": Integer manufacturer number,
 *         "Expiration": "Date string MM-YY",
 *         "StoreID": Integer store id
 *     },
 *     "Detection": {
 *         "SquareCount": Integer number of valid squares,
 *         "Hues": [Integer Hue, ...],
 *         "Results": [Integer TestStripResult, ...],
 *         "BinLabels": [ "String label", ...],
 *         "BinValues": [ "String values", ...]
 *     }
 * }
 * @endcode
 */
- (void)scanSuccess:(NSDictionary *)data;

/**
 * @brief frameError
 * @description This message is recieved when the identification pipeline has hit an error state and will
 * resume with a new frame.  This can be used to signal to the user pertinent information during
 * a scan.
 * @param error The error state that occurred.
 */
- (void)frameError:(TestStripFrameError)error;
/**
 * @brief frameStatus
 * This message is recieved when the identification pipeline has successfully passed one of several
 * steps.  This information can be relayed to the user to show scan progress.
 * @param status The status state that occurred.
 */
- (void)frameStatus:(TestStripFrameStatus)status;
// ... other methods here
-(void)reportSuccess;
-(void)reportFailed:(NSString*)message;
@end

/**
 * @brief TestStripWrapper
 * @description The wrapper object connecting Obj-C to the internal C++ library classes. This object should
 * be created and used to directly interact with the test strip library.
 */
@interface TestStripWrapper : NSObject {
    void *m_platform;
    id<TestStripLibDelegate> m_delegate;
}
@property (strong, nonatomic) AVCaptureVideoPreviewLayer  *previewLayer;
/**
 * @brief delegate
 * @description Getter for delegate object
 * @see TestStripLibDelegate
 * @return A reference to the current TestStripLibDelegate delegate object.
 */
- (id)delegate;
/**
 * @brief setDelegate
 * @decscription Setter for delegate object
 * @see TestStripLibDelegate
 * @param newDelegate The delegate object that should recieve status and error messages.
 */
- (void)setDelegate:(id)newDelegate;

/**
 * @brief initialize
 * @description Initialize scanning pipeline, including configuration of the camera.
 * Should be called once after applicationDidFinishLaunching
 */
- (void)initialize;
/**
 * @brief shutdown
 * @description Shutdown pipeline and clean up camera resources.
 */
- (void)shutdown;

/**
 * @brief startScan
 * @description Begin processing incoming frames.  Processing is resource intensive, and
 * should only be enabled when the app is in scan mode.
 */
- (void)startScan;
/**
 * @brief stopScan
 * @description Stop processing incoming frames.
 */
- (void)stopScan;

/**
 * @brief enableReporting
 * @description Enable or disable reporting calibration results to Azure
 */
- (void)enableReporting:(bool)enable;
- (void)enablePrintable:(bool)enable;

/**
 * @brief enableDemoMode
 * @description Enable or disable demo mode (turns flash on)
 */
- (void)enableDemoMode:(bool)enable;

/**
 * @brief enableOutdoorMode
 * @description Enable or disable outdoor mode (alternate detection)
 */
- (void)enableOutdoorMode:(bool)enable;

/**
 * @brief enableIndoorMode
 * @description Enable or disable indoor mode (alternate detection)
 */
- (void)enableIndoorMode:(bool)enable;

/**
 * @brief sendSetup
 * @description submit diagnostic setup for a group of reports
 * @param
 */
- (void)sendSetup:(NSString*)notes
    hardnessValue:(double)hardness
    cyanuricValue:(double)cyanuric
  alkalinityValue:(double)alkalinity
          phValue:(double)ph
    chlorineValue:(double)chlorine
   luminanceValue:(int)luminance;

/**
 * @brief sendReport
 * @description submit diagnostic report with passed metadata
 * @param
 */
- (void)sendReport:(bool)square1Valid
      square2Valid:(bool)square2Valid
      square3Valid:(bool)square3Valid
      square4Valid:(bool)square4Valid
      square5Valid:(bool)square5Valid
             notes:(NSString*)notes;

- (void)retryReport;
- (void)timer;
@end

