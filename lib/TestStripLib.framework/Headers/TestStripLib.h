//
//  TestStripLib.h
//  TestStripLib
//
//  Created by Systematic Group on 9/15/16.
//  Copyright © 2016 Systematic Group. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for TestStripLib.
FOUNDATION_EXPORT double TestStripLibVersionNumber;

//! Project version string for TestStripLib.
FOUNDATION_EXPORT const unsigned char TestStripLibVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <TestStripLib/PublicHeader.h>

#import "IOSPlatform.h"

