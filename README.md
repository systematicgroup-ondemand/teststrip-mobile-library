Mobile libraries for Digital Concepts' Test Strip project.  Here you will find the source, project files, example applications, and built releases for the iOS framework and Android apk.

# Integration #

### iOS ###
See the example application for reference.
PreviewController.mm contains all app integrations to library API.  API header can be referenced in include/IOSPlatform.h.
This header is normally contained in the Framework bundle, but is included for convenience.

* Grab a built framework from the Downloads section.
* In your Xcode project, drag the TestStripLib.framework into the "Linked Frameworks and Libraries".
* Ensure that the framework is also added to the "Embedded Binaries" section.
* Add a row to your info.plist for "Privacy - Camera Usage Description" with a value such as "Used to scan test strips" if compiling for iOS 10

### Android ###
See the example application for reference
PreviewActivity.java contains all app integrations to library API.  API header can be referenced in include/TestStripAdapter.java

* Use the built Android library from the Downloads section.
* In Android Studio click File > Import Module, click the ellipsis on the right of the text box and select the aar file downloaded above.
* Right click your application in the Project view, select Open Module Settings. 
* Go to the dependencies tab of your app module, click the green plus on the right and add a module dependency. Select the imported "testtrip-debug"


# Usage #

### Permissions ###
In both iOS and Android, the library will require the use of the back-facing camera.  The library handles requiring these permissions, but the app may need to provide information to the user on why the camera is required.

### Scanning ###
The app is responsible for the following:
* Instantiating the library.
* Setting a delegate class to listen for runtime events.
* Fetching the camera preview layer from the library wrapper to add to the UI.
* Starting and stopping scanning for strips.


### Detection ###
In your delegate class, there is an event "ScanSuccess" which will be called once a scan has detected a strip.  This callback is provided an argument containing all scan results information that may be relevant.  In iOS, this structure is detailed in iOSPlatform.h.  In Android the documentation is currently out of date.

#### Android ####
```Java
public void ScanSuccess(Map data) {
    int[] results = (int[])data.get("Results");
    for (int i=0; i<5; i++) {
        if (TestStripAdapter.TestStripResult.values()[results[i]] == TestStripAdapter.TestStripResult.Undefined) {
            //TODO
        }
    }
}
```
 
#### iOS ####
```Obj-C
- (void)scanSuccess:(NSDictionary *)data {
    //Keymap defined here to ensure order
    NSArray* keys = [Data[@"Detection"][@"Results"] allKeys];
    for (int i=0; i<[Data[@"SquarePos"] count]; i++) {
        if ([data[@"Detection"][@"Results"][keys[i]] integerValue] == TestStripResultUndefined) {
            //TODO
        }
	}
}
```


### Errata ###
The iOS and Android builds currently do not support x86/x86_64 architecture for simulators.  Testing and development is done on devices only for now.  
The Doxygen documentation under Docs is out of date.  Please refer to this README and sample code.