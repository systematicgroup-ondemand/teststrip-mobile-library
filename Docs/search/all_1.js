var searchData=
[
  ['badcolordeviation',['BadColorDeviation',['../enumcom_1_1dci_1_1teststrip_1_1_test_strip_adapter_1_1_frame_error.html#a92205ee5b2bc38e21685cbe9408b6bf3',1,'com::dci::teststrip::TestStripAdapter::FrameError']]],
  ['barcode1notdetected',['Barcode1NotDetected',['../enumcom_1_1dci_1_1teststrip_1_1_test_strip_adapter_1_1_frame_error.html#ad0453d953e605254d0f6cfe155a73899',1,'com::dci::teststrip::TestStripAdapter::FrameError']]],
  ['barcode2notdetected',['Barcode2NotDetected',['../enumcom_1_1dci_1_1teststrip_1_1_test_strip_adapter_1_1_frame_error.html#a0b9d42f3e541d86644dbac427d4bd5e5',1,'com::dci::teststrip::TestStripAdapter::FrameError']]],
  ['barcodebadangle',['BarcodeBadAngle',['../enumcom_1_1dci_1_1teststrip_1_1_test_strip_adapter_1_1_frame_error.html#aab34cc5c9773600f2f568af4009bccaa',1,'com::dci::teststrip::TestStripAdapter::FrameError']]],
  ['barcodemismatch',['BarcodeMismatch',['../enumcom_1_1dci_1_1teststrip_1_1_test_strip_adapter_1_1_frame_error.html#ac44ae5c7fe4bbed49feda9b2e979c9fa',1,'com::dci::teststrip::TestStripAdapter::FrameError']]],
  ['barcodetoobig',['BarcodeTooBig',['../enumcom_1_1dci_1_1teststrip_1_1_test_strip_adapter_1_1_frame_error.html#a7b8c97784460bc59bc19a1b2d8b56d0e',1,'com::dci::teststrip::TestStripAdapter::FrameError']]],
  ['barcodetoosmall',['BarcodeTooSmall',['../enumcom_1_1dci_1_1teststrip_1_1_test_strip_adapter_1_1_frame_error.html#a18a2b669a6a427c97c7b33e7dd331d4d',1,'com::dci::teststrip::TestStripAdapter::FrameError']]]
];
